<?php

namespace Drupal\custom_rest_api\Plugin\rest\resource;

use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Provides a resource to get continent names, country names and countries by ID.
 *
 * @RestResource(
 *   id = "custom_rest_api endpoint",
 *   label = @Translation("Custom REST API endpoint"),
 *   uri_paths = {
 *     "canonical" = "/rest/custom"
 *   }
 * )
 */
class CustomEndPoint extends ResourceBase {

  /**
   * @returns JsonResponse
   */
  public function getDrupalContinents() {
    $continents = Vocabulary::loadMultiple();
    return new JsonResponse($continents);
  }

  /**
   * @return JsonResponse
   */
  public function getDrupalCountries() {
    $countries = Term::loadMultiple();
    foreach ($countries as $country) {
      $c[] = $country->getName();
    }
    return new JsonResponse($c);
  }

  /**
   * @param $id
   * @returns Country by ID.
   */
  public function getCountryById($id) {
    $country = Term::load($id);
    return new JsonResponse($country->getName());
  }
}
